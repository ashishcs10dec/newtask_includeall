﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace calculator
{
    public class Factory
    {
        public String NormalizeString(String WriteLineText)
        {
            if (WriteLineText.ToLower().Contains("\\\\"))
                WriteLineText = WriteLineText.Replace("\\\\", "");
            if (WriteLineText.ToLower().Contains(";"))
                WriteLineText = WriteLineText.Replace(";", ",");
            if (WriteLineText.ToLower().Contains("\\n"))
                WriteLineText = WriteLineText.Replace("\\n", "");
            return WriteLineText;
        }
    }
}
