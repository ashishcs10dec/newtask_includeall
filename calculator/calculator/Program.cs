﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                int Sum = 0;
                String NegativeNumbers = "";
                String GreaterThan = "";
                Console.WriteLine("Please enter the format:");
                String WriteLineText = Console.ReadLine().Trim();
                Factory F = new Factory();
                if (WriteLineText.ToLower().Contains("sum"))
                {
                    WriteLineText = WriteLineText.Remove(0, 3).Trim();
                    WriteLineText = F.NormalizeString(WriteLineText);
                    if (WriteLineText.Contains(","))
                    {
                        String[] ExistElement = WriteLineText.Split(',');
                        if (ExistElement.Count() > 1)
                        {
                            for (int i = 0; i < ExistElement.Count(); i++)
                            {
                                if (ExistElement[i] != "")
                                {
                                    if (Convert.ToInt32(ExistElement[i]) < 0)
                                    {
                                        if (NegativeNumbers == "")
                                            NegativeNumbers = ExistElement[i];
                                        else
                                            NegativeNumbers = NegativeNumbers + "," + ExistElement[i];
                                    }
                                    else if (Convert.ToInt32(ExistElement[i]) < 1000)
                                    {
                                        if (Sum == 0)
                                            Sum = Convert.ToInt32(ExistElement[i]);
                                        else
                                            Sum = Sum + Convert.ToInt32(ExistElement[i]);
                                    }
                                    else
                                        if (GreaterThan == "")
                                            GreaterThan = ExistElement[i];
                                        else
                                            GreaterThan = GreaterThan + "," + ExistElement[i];
                                }
                            }
                        }
                    }
                    else
                        Sum = Convert.ToInt32(WriteLineText);
                }
                else if (WriteLineText.ToLower().Contains("multiply"))
                {
                    WriteLineText = WriteLineText.Remove(0, 8).Trim();
                    WriteLineText = F.NormalizeString(WriteLineText);
                    if (WriteLineText.Contains(","))
                    {
                        String[] ExistElement = WriteLineText.Split(',');
                        if (ExistElement.Count() > 1)
                        {
                            for (int i = 0; i < ExistElement.Count(); i++)
                            {
                                if (ExistElement[i] != "")
                                {
                                    if (Convert.ToInt32(ExistElement[i]) < 0)
                                    {
                                        if (NegativeNumbers == "")
                                            NegativeNumbers = ExistElement[i];
                                        else
                                            NegativeNumbers = NegativeNumbers + "," + ExistElement[i];
                                    }
                                    else if (Convert.ToInt32(ExistElement[i]) < 1000)
                                    {
                                        if (Sum == 0)
                                            Sum = Convert.ToInt32(ExistElement[i]);
                                        else
                                            Sum = Sum * Convert.ToInt32(ExistElement[i]);
                                    }
                                    else
                                        if (GreaterThan == "")
                                            GreaterThan = ExistElement[i];
                                        else
                                            GreaterThan = GreaterThan + "," + ExistElement[i];
                                }
                            }
                        }
                    }
                    else
                        Sum = Convert.ToInt32(WriteLineText);
                }
                if (NegativeNumbers != "")
                    Console.WriteLine("Negative numbers(" + NegativeNumbers + ") are not allowed!");
                else if (GreaterThan == "")
                    Console.WriteLine(Sum);
                else
                    Console.WriteLine(Sum + Environment.NewLine + "The numbers(" + GreaterThan + ") are greater than 1000.");
                Console.ReadLine();
            }
            catch
            {
                Console.WriteLine("Something went wrong!");
                Console.ReadLine();
            }
        }
    }
}
